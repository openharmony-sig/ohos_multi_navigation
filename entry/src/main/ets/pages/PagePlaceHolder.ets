/**
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MultiNavPathStack, SplitPolicy } from '@ohos/multinavigation';
import { hilog } from '@kit.PerformanceAnalysisKit';
import window from '@ohos.window';
import { promptAction } from '@kit.ArkUI';

@Component
export struct PagePlaceHolder {
  @State message: string = 'PagePlaceHolder';
  @Consume('pageStack') pageStack: MultiNavPathStack;
  controller: TextInputController = new TextInputController()
  text: String = '';
  lastBackTime: number = 0;

  build() {
    if (this.log()) {
      NavDestination() {
        Column() {
          Column() {
            Text(this.message)
              .fontSize(50)
              .fontWeight(FontWeight.Bold)
          }
          .width('100%')
          .height('8%')

          Stack({alignContent: Alignment.Center}) {
            Text('PlaceHolder示例页面')
              .fontSize(80)
              .fontWeight(FontWeight.Bold)
              .fontColor(Color.Red)
          }
          .width('100%')
          .height('92%')
        }
      }
      .hideTitleBar(true)
      .onBackPressed(() => {
        const currentTime = Date.now();
        hilog.info(0x0000, 'demotest', 'PagePlaceHolder onBackPressed: currentTime=' + currentTime);
        let size = this.pageStack.size();
        if (size === 2) {
          if (currentTime - this.lastBackTime < 1500) {
            const windowStage = AppStorage.get<window.WindowStage>('myMainWindow');
            if (windowStage !== undefined) {
              try {
                hilog.info(0x0000, 'demotest', 'PagePlaceHolder hideWithAnimation: ');
                windowStage.getMainWindowSync().minimize();
              } catch (e) {
                hilog.info(0x0000, 'demotest', 'PagePlaceHolder hide fail: ');
                this.lastBackTime = currentTime;
                return false;
              }
              this.lastBackTime = 0;
              return true;
            }
          } else {
            this.lastBackTime = currentTime;
            promptAction.showToast({ message: '再按一次退出' });
            return true;
          }
        }
        return false;
      })
    }
  }

  log(): boolean {
    hilog.info(0x0000, 'demotest', 'PagePlaceHolder build called');
    return true;
  }
}