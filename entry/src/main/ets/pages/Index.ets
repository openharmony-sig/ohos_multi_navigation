/**
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MultiNavigation, MultiNavPathStack, SplitPolicy } from '@ohos/multinavigation';
import { PageDetail1 } from './PageDetail1';
import { PageDetail2 } from './PageDetail2';
import { PageFull1 } from './PageFull1';
import { PageHome1 } from './PageHome1';
import { hilog } from '@kit.PerformanceAnalysisKit';
import { PagePlaceHolder } from './PagePlaceHolder';

@Entry
@Component
struct Index {
  private mPolicyMap = new Map<string, SplitPolicy>();
  @Provide('pageStack') pageStack: MultiNavPathStack = new MultiNavPathStack();

  @Builder
  PageMap(name: string, param?: object) {
    if (name === 'PageHome1') {
      PageHome1({ param: param })
    } else if (name === 'PageDetail1') {
      PageDetail1({ param: param });
    } else if (name === 'PageDetail2') {
      PageDetail2({ param: param });
    } else if (name === 'PageFull1') {
      PageFull1();
    } else if (name === 'PagePlaceHolder') {
      PagePlaceHolder();
    }
  }

  aboutToAppear(): void {
    // this.mPolicyMap.set('PageHome1', SplitPolicy.HOME_PAGE);
    // this.mPolicyMap.set('PageFull1', SplitPolicy.FULL_PAGE);
    // this.pageStack.setPagePolicy(this.mPolicyMap);
    this.pageStack.pushPath({
      name: 'PageHome1',
      param: "paramTest",
      onPop: (popInfo) => {
        hilog.info(0x0000, 'demotest', 'PageHome1 onpop popInfo.result=' + popInfo.result);
      }
    }, false, SplitPolicy.HOME_PAGE);
    this.pageStack.setKeepBottomPage(true);
  }

  build() {
    Column() {
      Row() {
        MultiNavigation({
          navDestination: this.PageMap,
          multiStack: this.pageStack,
          onHomeShowInTop: (name) => {
            hilog.info(0x0000, 'demotest', 'index onHomeShowInTop name=' + name);
          },
        })
      }
      .width('100%')
    }
  }
}